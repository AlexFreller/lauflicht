/*
 * counter.h
 *
 * Created: 16.01.2015 13:23:02
 *  Author: Luu
 */ 


#ifndef COUNTER_H_
#define COUNTER_H_

#include <stdbool.h>

extern void CntInit();

extern void CntInkrement();

extern uint8_t CntGetValue();



#endif /* COUNTER_H_ */