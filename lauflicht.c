/*
 * Lauflicht.c
 *
 * Created: 16.01.2015 13:21:58
 *  Author: Luu
 */ 

#include <avr/interrupt.h>
#include <avr/io.h>
#include "led.h"
#include "counter.h"

#define TIMER_0_CNT 0x91

uint8_t counter;
static uint8_t timer;
ISR(TIMER0_OVF_vect)
  /* Interrupt Aktion alle
  (8000000/256)/256 Hz = 122,0703125 Hz
  bzw.
  1/122,0703125 s = 8,192 ms  
  */
{
	timer++;
	if (timer == 122)
	{
		timer = 0;
		CntInkrement();
	}
	TCNT0 = TIMER_0_CNT;  // reset counter
	       
}

int main(void)
{
	// Timer 0 konfigurieren
	TCCR0 = (1<<CS02); // Prescaler 256
 
	// Overflow Interrupt erlauben
	TIMSK |= (1<<TOIE0);
 
	// Global Interrupts aktivieren
	sei();
	
	CntInit();
	timer = 0;
	
	counter = 1;
	
	uint8_t bytes[15];
	
	bytes[0]=0x80;
	bytes[1]=0xC0;
	bytes[2]=0xE0;
	bytes[3]=0xF0;
	bytes[4]=0xF8;
	bytes[5]=0xFC;
	bytes[6]=0xFE;
	bytes[7]=0xFF;
	bytes[8]=0x7F;
	bytes[9]=0x3F;
	bytes[10]=0x1F;
	bytes[11]=0x0F;
	bytes[12]=0x07;
	bytes[13]=0x03;
	bytes[14]=0x01;
	
	
    while(1)
    {
		LedSet(bytes[CntGetValue() - 1]);
    }
}