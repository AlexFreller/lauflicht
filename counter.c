/*
 * counter.c
 *
 * Created: 16.01.2015 13:23:18
 *  Author: Luu
 */ 
#include "counter.h"
#include <avr/io.h>

static uint8_t counter;
static const int Resetwert = 16;

void CntInit()
{
	counter = 1;
}

void CntInkrement()
{
	counter++;
	if (counter == Resetwert)
		counter = 0;
}

uint8_t CntGetValue()
{
	return counter;
}