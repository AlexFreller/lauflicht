﻿/*
 * led.c
 *
 * Created: 16.01.2015 13:45:21
 *  Author: Alexander
 */

#include <avr/io.h>
#include "led.h"


void LedInit()
{
	DDRB = 0xff;
}
void LedOn(int lednr)
{
	if (lednr >= 0 && lednr <= 7)
	{
		PORTB |= (1<< lednr);
	}
}
void LedOff(int lednr)
{
	if (lednr >= 0 && lednr <= 7)
	{
		PORTB &= ~(1<< lednr);
	}
}
bool LedCheck(int lednr)
{
	if (lednr >= 0 && lednr <= 7)
	{
		return PORTB & (1<<lednr);
	}
}
void LedToggle(int lednr)
{
	PORTB ^= (1<<lednr);
}
void LedSet(uint8_t zustand)
{
	PORTB = zustand;
}