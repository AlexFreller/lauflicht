/*
 * led.h
 *
 * Created: 16.01.2015 13:35:30
 * Author: Alexander
 */ 


#ifndef LED_H_
#define LED_H_

#include <stdbool.h>

extern void LedInit();

extern void LedOn(int lednr);

extern void LedOff(int lednr);

extern bool LedCheck(int lednr);

extern void LedToggle(int lednr);

extern void LedSet(uint8_t zustand);
#endif /* LED_H_ */